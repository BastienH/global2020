#ifndef SOLS_HPP
#define SOLS_HPP

#include "data.hpp"

class Sol
{
  private:

    sf::RenderWindow& _window;
    TYPEBUG _type;

    sf::Texture _textureSol;
    sf::Sprite _spriteSol;

  public:
    Sol(sf::RenderWindow& window, const TYPEBUG& type);
    virtual ~Sol (){};
    void draw(sf::RenderTexture& tx);
    void tick();
    void setPosition(const int& x, const int& y);

    const TYPEBUG& getType() const;
};

#endif
