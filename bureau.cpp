#include "bureau.hpp"

Bureau * Bureau::_bureau = nullptr;

Bureau::Bureau(sf::RenderWindow& window)
: _window(window), _terminal(nullptr), _solution(nullptr), _maxCount(0), _count(0), _popup(false), _popupTime(0), _nbBugs(0), _frameBug(60), _frameBugMax(10 * 60), _screen(sf::Vector2f(wWindow, hWindow - hBarre)), _connected(true), _sound(new Sound()), _score(0), _start(false), _flou(false)
{
  _textureBackground.loadFromFile("./assets/bureau.png");
  _spriteBackground.setTexture(_textureBackground);

  _texturePopup.loadFromFile("./assets/popup.png");
  _spritePopup.setTexture(_texturePopup);
  _spritePopup.setPosition(sf::Vector2f(20, xConsole + wConsole + 20));

  _screen.setPosition(sf::Vector2f(0, 0));
  _screen.setFillColor(sf::Color::Black);


  _textureWindow.create(wWindow, hWindow);

  //_spriteWindow.setScale(sf::Vector2f(1, -1));

  //_spriteWindow.move(0, hWindow);

  //std::cout << taille_mat_x << " " << taille_mat_y << std::endl;

  //init de la matrice
  _bureau = this;

  for(int i = 0; i < taille_mat_x; ++i)
  {
    for(int j = 0; j < taille_mat_y; ++j)
    {
      _matrice[i][j] = nullptr;
    }
  }

  _font.loadFromFile("./assets/arial.ttf");

  _textScore.setFont(_font);
  _textScore.setCharacterSize(20);
  _textScore.setFillColor(sf::Color::Black);
  _textScore.setPosition(sf::Vector2f(wWindow - wButton, hWindow - hBarre + 2));

  _textMotBonus.setFont(_font);
  _textMotBonus.setCharacterSize(20);
  _textMotBonus.setFillColor(sf::Color::Black);
  _textMotBonus.setPosition(sf::Vector2f(wWindow - 6 * wButton, hWindow - hBarre + 2));

  pickNewBonus();

  //_matrice[2][2] = new Bug(_window, 2, 2, FLOU);

}

Bureau::~Bureau()
{
}

void Bureau::deleteAll()
{
  delete _terminal;
  delete _solution;
  delete _sound;

  for(int i = 0; i < taille_mat_x; ++i)
  {
    for(int j = 0; j < taille_mat_y; ++j)
    {
      delete _matrice[i][j];
    }
  }

  //delete _bureau;
}

void Bureau::draw()
{
  _textureWindow.clear(sf::Color::Black);

  _textureWindow.draw(_spriteBackground);


  for(int i = 0; i < taille_mat_x; ++i)
  {
    for(int j = 0; j < taille_mat_y; ++j)
    {
      if(_matrice[i][j])
      {
        _matrice[i][j]->draw(_textureWindow);
      }
    }
  }

  if(_solution)
  {
    _solution->draw(_textureWindow);
  }

  if(!_connected)
  {
    _textureWindow.draw(_screen);
  }

  if(_popup && _popupTime >= 0)
  {
    _textureWindow.draw(_spritePopup);
    --_popupTime;
  }

  if(_terminal)
  {
    _terminal->draw(_textureWindow);
  }

  writeScore();

  _textureWindow.draw(_textMotBonus);

  _textureWindow.display();

  if(_flou)
  {
    createblur(_textureWindow, 3, 1);
  }

  //_spriteWindow.setScale(sf::Vector2f(1, -1));

  _spriteWindow.setTexture(_textureWindow.getTexture());


  _window.draw(_spriteWindow);
}

void Bureau::tick()
{
  draw();

  if(_start)
  {
    generateBug();
  }

  if(_count <= 0)
  {
    getEvents();

    _count = _maxCount;
  }
  else
  {
    --_count;
  }

  tickBugs();

}

Bureau * Bureau::getBureau()
{
  return _bureau;
}

void Bureau::getEvents()
{
  sf::Event event;

  while (_window.pollEvent(event))
  {
    // fermeture de la fenêtre lorsque l'utilisateur le souhaite
    switch (event.type)
    {
        // fenêtre fermée
        case sf::Event::Closed:
            _window.close();
            break;

        // touche pressée
        case sf::Event::KeyPressed:

          if(_terminal)
          {
            _terminal->readCommande(event);
          }

          break;

          case sf::Event::MouseButtonReleased:

            clickReleased(event);

            break;

          case sf::Event::MouseMoved:

            mouseMoved(event);

            break;


        // on ne traite pas les autres types d'évènements
        default:
            break;
    }
  }
}

void Bureau::clickReleased(sf::Event& event)
{
  int xClick = event.mouseButton.x;
  int yClick = event.mouseButton.y;
  int i, j;

  //si clic dans la barre

  if(yClick > hWindow - hBarre && yClick < hWindow)
  {
    if(xClick > 0 && xClick < wButton)
    {
      _window.close();
    }
    else
    {
      if(_terminal == nullptr)
      {
        _terminal = new Terminal(_window);
        _sound->play(OPENTERM);
      }
    }
  }
  else if(xClick > xConsole + wConsole - 20 && xClick < xConsole + wConsole && yClick > yConsole && yClick < yConsole + 20)
  {
    //std::cout << "close" << std::endl;
    killTerminal();
  }
  else if(xClick > xAffichage && xClick < wWindow && yClick > 0 && yClick < hWindow - hBarre)
  {
    j = yClick / tailleBug;
    i = (xClick - xAffichage) / tailleBug;

    //std::cout << i << " " << j << std::endl;

    if(_matrice[i][j] && _solution && _solution->getType() == _matrice[i][j]->getType())
    {
      switch (_matrice[i][j]->getType())
      {
        case FREEZE:
          _popup = true;
          _popupTime = popupTimeMax;
          _matrice[i][j]->solve();
          delete _matrice[i][j];
          _matrice[i][j] = nullptr;

          --_nbBugs;

          ++_score;

          break;
        case VIRUS:
          if(_terminal)
          {
            _terminal->writeLine("i : " + std::to_string(i) + " j : " + std::to_string(j) + "\n");
          }
          else
          {
            delete _solution;
            _solution = nullptr;
          }
          break;
        case VPN:
          break;
        case BONUS:
          break;
        case FAKEINPUT:
          break;
        case FLOU:
          _popup = true;
          _popupTime = popupTimeMax;
          _matrice[i][j]->solve();
          delete _matrice[i][j];
          _matrice[i][j] = nullptr;

          --_nbBugs;

          ++_score;

          break;
      }
    }
    else
    {
      _sound->play(FAIL);
    }
  }
  else
  {
    _sound->play(FAIL);
  }
}


void Bureau::mouseMoved(const sf::Event& event)
{
  if(_solution)
  {
    _solution->setPosition(event.mouseMove.x - tailleSol/2, event.mouseMove.y - tailleSol/2);
  }

}

void Bureau::execCommande(const std::string& commande)
{
  //std::cout << commande.substr(0,4) << std::endl;
  if(commande == "flame")
  {
    if(_solution)
    {
      delete _solution;
    }

    _solution = new Sol(_window, FREEZE);

  }
  else if(commande == "getpid")
  {
    if(_solution)
    {
      delete _solution;
    }

    _solution = new Sol(_window, VIRUS);
  }
  else if(commande.substr(0,4) == "kill")
  {
    int i = -1;
    int j = -1;

    try
    {
      i = std::stoi(commande.substr(5, 1));
      j = std::stoi(commande.substr(7, 1));
    }
    catch(const std::invalid_argument& e)
    {
      _sound->play(FAIL);
    }

    //std::cout << commande.substr(5, 1) << " " << commande.substr(7, 1) << std::endl;
    if(i < taille_mat_x && i >= 0 && j >= 0 && j < taille_mat_y && _matrice[i][j])
    {
      _popup = true;
      _popupTime = popupTimeMax;
      _matrice[i][j]->solve();
      delete _matrice[i][j];
      _matrice[i][j] = nullptr;

      --_nbBugs;

      ++_score;
    }
  }
  else if(commande == "call cri")
  {
    _connected = true;
    _popup = true;
    _popupTime = popupTimeMax;

    _sound->play(CONNECT);

    for(int i = 0; i < taille_mat_x; ++i)
    {
      for(int j = 0; j < taille_mat_y; ++j)
      {
        if(_matrice[i][j] && _matrice[i][j]->getType() == VPN)
        {
          _matrice[i][j]->solve();
          delete _matrice[i][j];
          _matrice[i][j] = nullptr;

          --_nbBugs;

          ++_score;
        }
      }
    }

  }
  else if(commande == "help")
  {
    _terminal->writeHelp();
  }
  else if(commande.substr(0, 3) == "man")
  {
    if(commande.substr(4, commande.size()) == "freeze")
    {
      _terminal->writeMan(FREEZE);
    }
    else if(commande.substr(4, commande.size()) == "virus")
    {
      _terminal->writeMan(VIRUS);
    }
    else if(commande.substr(4, commande.size()) == "vpn")
    {
      _terminal->writeMan(VPN);
    }
    else if(commande.substr(4, commande.size()) == "bonus")
    {
      _terminal->writeMan(BONUS);
    }
    else if(commande.substr(4, commande.size()) == "fakeinput")
    {
      _terminal->writeMan(FAKEINPUT);
    }
    else if(commande.substr(4, commande.size()) == "flou")
    {
      _terminal->writeMan(FLOU);
    }

  }
  else if(commande == "start")
  {
    _start = true;
  }
  else if(commande == "clear")
  {
    _terminal->clear();
  }
  else if(commande == "close connection")
  {
    for(int i = 0; i < taille_mat_x; ++i)
    {
      for(int j = 0; j < taille_mat_y; ++j)
      {
        if(_matrice[i][j] && _matrice[i][j]->getType() == FAKEINPUT)
        {
          _matrice[i][j]->solve();
          delete _matrice[i][j];
          _matrice[i][j] = nullptr;

          --_nbBugs;

          ++_score;
        }
      }
    }
  }
  else if(commande == _motBonus)
  {
    if(_start)
    {
      ++_score;
      pickNewBonus();
    }
  }
  else if(commande == "glasses")
  {
    if(_solution)
    {
      delete _solution;
    }

    _solution = new Sol(_window, FLOU);
  }

}

void Bureau::setFramerate(const int& frame)
{
    _maxCount = frame;
}

const int& Bureau::getFramerate() const
{
  return _maxCount;
}

void Bureau::tickBugs()
{
  for(int i = 0; i < taille_mat_x; ++i)
  {
    for(int j = 0; j < taille_mat_y; ++j)
    {
      if(_matrice[i][j])
      {
        _matrice[i][j]->tick();
      }
    }
  }
}

void Bureau::killTerminal()
{
  delete _terminal;
  _terminal = nullptr;
  _sound->play(CLOSETERM);
}

void Bureau::generateBug()
{
  int i, j, iter = 0, type;
  TYPEBUG bug;

  if(_frameBug <= 0)
  {
    do
    {
      i = rand()%taille_mat_x;
      j = rand()%taille_mat_y;
      type = rand()%5;
      //std::cout << type << std::endl;
      ++iter;
    }  while(iter < 100 && _matrice[i][j]);

    if(iter < 100)
    {
      switch (type)
      {
        case FREEZE:
          _sound->play(SFREEZE);
          bug = FREEZE;
          break;
        case VIRUS:
          _sound->play(ERROR);
          bug = VIRUS;
          break;
        case VPN:
          bug = VPN;
          break;
        case FAKEINPUT:
          _sound->play(FAKECONNECTION);
          bug = FAKEINPUT;
          break;
        case FLOU:
          //_sound->play(FLOU);
          bug = FLOU;
          break;
      }

      _matrice[i][j] = new Bug(_window, i, j, bug);

      _frameBug = _frameBugMax;
      _frameBugMax -= 10;

      ++_nbBugs;
    }
  }
  else
  {
    --_frameBug;
  }
}

void Bureau::disconnect()
{
  _connected = false;
}

const int& Bureau::getNbBug() const
{
  return _nbBugs;
}

Sound * Bureau::getSound()
{
  return _sound;
}

const bool& Bureau::isConnected() const
{
  return _connected;
}

void Bureau::writeScore()
{
  _textScore.setString("Score : " + std::to_string(_score));
  _textureWindow.draw(_textScore);
}

void Bureau::pickNewBonus()
{
  std::ifstream ifs("./assets/motsBonus.json");
  Json::Reader reader;
  Json::Value obj;
  reader.parse(ifs, obj); // reader can also read strings

  const Json::Value& mot = obj["mots"];

  int nb = rand()%mot.size();

  _motBonus = mot[nb]["mot"].asString();

  _textMotBonus.setString("Mot bonus : " + _motBonus);
}

void Bureau::fakeInput()
{
  if(_terminal)
  {
    _terminal->fakeInput();
  }
}

//test flou


void createblur(sf::RenderTexture &renderTexture, int boxSize, int iterations)
{
  for (int n = 0; n < iterations; ++n)
  {
    sf::Image img = renderTexture.getTexture().copyToImage();
    sf::Image _new;
    _new.create(img.getSize().x, img.getSize().y);

    for (unsigned int i = 0; i < img.getSize().x; ++i)
    {
      for (unsigned int j = 0; j < img.getSize().y; ++j)
      {

        int sum = 0, r = 0, g = 0, b = 0, a = 0;
        for (int ii = -boxSize / 2; ii < boxSize / 2; ++ii)
        {
          for (int jj = -boxSize / 2; jj < boxSize / 2; ++jj)
          {
            if (sf::IntRect(0, 0, img.getSize().x, img.getSize().y).contains(sf::Vector2i(ii + i, jj + j)))
            {
              sum += 1;
              sf::Color color = img.getPixel(i + ii, j + jj);
              r += color.r;
              b += color.b;
              g += color.g;
              a += color.a;
            }
          }
        }
        if (sum == 0)
                _new.setPixel(i, j, sf::Color(0, 0, 0, 0));
        else
                _new.setPixel(i, j, sf::Color((int)(r / sum), (int)(g / sum), (int)(b / sum), (int)(a / sum)));
      }
    }
    renderTexture.clear();
    sf::Texture texture;
    texture.loadFromImage(_new);
    sf::Sprite sprite(texture);
    renderTexture.draw(sprite);
    renderTexture.display();
  }
}

void Bureau::flouter()
{
  _flou = true;
}

void Bureau::deflouter()
{
  _flou = false;
}
