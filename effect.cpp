#include "effect.hpp"
#include "bureau.hpp"


void Freeze::tick()
{
  int modif = 5;
  int newFrameRate;

  if(_count <= 0)
  {
    newFrameRate = Bureau::getBureau()->getFramerate() + modif;

    if(newFrameRate <= 100)
    {
      //std::cout << newFrameRate << std::endl;
      Bureau::getBureau()->setFramerate(newFrameRate);
    }

    _count = _maxCount;
  }
  else
  {
    --_count;
  }
}

void Freeze::solve()
{
  Bureau::getBureau()->setFramerate(0);
}


void Virus::tick()
{
  if(_count <= 0)
  {
    //Bureau::getBureau()->getSound()->play(ERROR);
    Bureau::getBureau()->killTerminal();
    _count = _maxCount;
  }
  else
  {
    --_count;
  }
}

void Virus::solve()
{
}

void Vpn::tick()
{
  if(Bureau::getBureau()->isConnected())
  {
    Bureau::getBureau()->getSound()->play(DISCONNECT);
    Bureau::getBureau()->disconnect();
  }
}

void Vpn::solve()
{
}

void FakeInput::tick()
{
  if(_count <= 0)
  {
    //Bureau::getBureau()->getSound()->play(ERROR);
    Bureau::getBureau()->fakeInput();
    _count = _maxCount;
  }
  else
  {
    --_count;
  }
}

void FakeInput::solve()
{
}

void Flou::tick()
{
  Bureau::getBureau()->flouter();
}

void Flou::solve()
{
  Bureau::getBureau()->deflouter();
}
