#include "bugs.hpp"

//bugs

Bug::Bug(sf::RenderWindow& window, const int& i, const int& j, const TYPEBUG& type)
: _window(window), _effect(nullptr), _type(type)
{
  switch (type)
  {
    case FREEZE:
      _textureBug.loadFromFile("./assets/freeze.png");
      _effect = new Freeze(_window);
      break;
    case VIRUS:
      _textureBug.loadFromFile("./assets/virus.png");
      _effect = new Virus(_window);
      break;
    case VPN:
      _textureBug.loadFromFile("./assets/virus.png");
      _effect = new Vpn(_window);
      break;
    case FAKEINPUT:
      _textureBug.loadFromFile("./assets/fake.png");
      _effect = new FakeInput(_window);
      break;
    case FLOU:
      _textureBug.loadFromFile("./assets/flou.png");
      _effect = new Flou(_window);
      break;
  }

  _spriteBug.setTexture(_textureBug);
  _spriteBug.setPosition(sf::Vector2f(xAffichage + i*tailleBug, j*tailleBug));
}

Bug::~Bug()
{
  delete _effect;
}


void Bug::tick()
{
  _effect->tick();
}

void Bug::draw(sf::RenderTexture& tx)
{
  tx.draw(_spriteBug);
}

void Bug::solve()
{
  _effect->solve();
}

const TYPEBUG& Bug::getType() const
{
  return _type;
}
