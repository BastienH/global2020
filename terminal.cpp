#include "terminal.hpp"
#include "bureau.hpp"


Terminal::Terminal(sf::RenderWindow& window)
: _window(window), _commande(""), _save(""), _nbLines(0)
{
  _textureTerminal.loadFromFile("./assets/console.png");
  _spriteTerminal.setTexture(_textureTerminal);
  _spriteTerminal.setPosition(sf::Vector2f(xConsole, yConsole));

  _font.loadFromFile("./assets/arial.ttf");

  _textCommande.setFont(_font);
  _textCommande.setCharacterSize(10);
  _textCommande.setFillColor(sf::Color::White);
  _textCommande.setPosition(sf::Vector2f(xConsole + 14, yText + 3));


  _textSave.setFont(_font);
  _textSave.setCharacterSize(10);
  _textSave.setFillColor(sf::Color::White);
  _textSave.setPosition(sf::Vector2f(xConsole + 12, yConsole + 21));
}

Terminal::~Terminal()
{

}

void Terminal::draw(sf::RenderTexture& tx)
{
  tx.draw(_spriteTerminal);
  tx.draw(_textCommande);
  tx.draw(_textSave);
}

void Terminal::readCommande(const sf::Event& event)
{
  if(_commande.size() < 20)
  {
    switch (event.key.code)
    {
      //ligne 1

      case sf::Keyboard::A:
      {
        _commande += "a";
        break;
      }

      case sf::Keyboard::Z:
      {
        _commande += "z";
        break;
      }

      case sf::Keyboard::E:
      {
        _commande += "e";
        break;
      }

      case sf::Keyboard::R:
      {
        _commande += "r";
        break;
      }

      case sf::Keyboard::T:
      {
        _commande += "t";
        break;
      }

      case sf::Keyboard::Y:
      {
        _commande += "y";
        break;
      }

      case sf::Keyboard::U:
      {
        _commande += "u";
        break;
      }

      case sf::Keyboard::I:
      {
        _commande += "i";
        break;
      }

      case sf::Keyboard::O:
      {
        _commande += "o";
        break;
      }

      case sf::Keyboard::P:
      {
        _commande += "p";
        break;
      }

      //ligne 2

      case sf::Keyboard::Q:
      {
        _commande += "q";
        break;
      }

      case sf::Keyboard::S:
      {
        _commande += "s";
        break;
      }

      case sf::Keyboard::D:
      {
        _commande += "d";
        break;
      }

      case sf::Keyboard::F:
      {
        _commande += "f";
        break;
      }

      case sf::Keyboard::G:
      {
        _commande += "g";
        break;
      }

      case sf::Keyboard::H:
      {
        _commande += "h";
        break;
      }

      case sf::Keyboard::J:
      {
        _commande += "j";
        break;
      }

      case sf::Keyboard::K:
      {
        _commande += "k";
        break;
      }

      case sf::Keyboard::L:
      {
        _commande += "l";
        break;
      }

      case sf::Keyboard::M:
      {
        _commande += "m";
        break;
      }

      //ligne 3

      case sf::Keyboard::W:
      {
        _commande += "w";
        break;
      }

      case sf::Keyboard::X:
      {
        _commande += "x";
        break;
      }

      case sf::Keyboard::C:
      {
        _commande += "c";
        break;
      }

      case sf::Keyboard::V:
      {
        _commande += "v";
        break;
      }

      case sf::Keyboard::B:
      {
        _commande += "b";
        break;
      }

      case sf::Keyboard::N:
      {
        _commande += "n";
        break;
      }

      case sf::Keyboard::Space:
      {
        _commande += " ";
        break;
      }

      //nb


      case sf::Keyboard::Numpad0:
      {
        _commande += "0";
        break;
      }

      case sf::Keyboard::Numpad1:
      {
        _commande += "1";
        break;
      }

      case sf::Keyboard::Numpad2:
      {
        _commande += "2";
        break;
      }

      case sf::Keyboard::Numpad3:
      {
        _commande += "3";
        break;
      }

      case sf::Keyboard::Numpad4:
      {
        _commande += "4";
        break;
      }

      case sf::Keyboard::Numpad5:
      {
        _commande += "5";
        break;
      }

      case sf::Keyboard::Numpad6:
      {
        _commande += "6";
        break;
      }

      case sf::Keyboard::Numpad7:
      {
        _commande += "7";
        break;
      }

      case sf::Keyboard::Numpad8:
      {
        _commande += "8";
        break;
      }

      case sf::Keyboard::Numpad9:
      {
        _commande += "9";
        break;
      }

      default:
      break;

    }
  }

  switch (event.key.code)
  {
    //entrée + espace
    case sf::Keyboard::Return:
    {
      //std::cout << _commande << std::endl;
      exec();
      break;
    }

    case sf::Keyboard::BackSpace:
    {
      if(_commande != "")
        _commande.erase(_commande.size() - 1, 1);
      break;
    }

    default:
      break;
  }

  //std::cout << _commande << std::endl;
  _textCommande.setString(_commande);

}

void Terminal::delFirstLine(std::string& txt)
{
  int lg = 0;

  while(txt[lg] != '\n')
  {
    ++lg;
  }

  txt.erase(0, lg + 1);

  --_nbLines;
}

void Terminal::exec()
{
  if(_commande != "")
  {
    _save += _commande + "\n";
    _textSave.setString(_save);
    Bureau::getBureau()->execCommande(_commande);
    _commande = "";
    ++_nbLines;
    if(_nbLines >= 14)
    {
      delFirstLine(_save);
    }
  }
}

void Terminal::writeHelp()
{
  int nb = 9;
  std::string help = "flame : unfreeze pc \nglasses : remove blur\ngetpid : getPid of process\nkill i j : kill process with the pid\ncall cri : solve vpn issues\nclose connection : solve fake input\nman (freeze|virus|vpn|bonus|fakeinput|flou)\nstart : start the game\nclear : clear terminal\n";

  //std::cout << help << std::endl;

  while(_nbLines >= 14 - nb)
  {
    delFirstLine(_save);
  }

  _nbLines += nb;

  _save += help;
  _textSave.setString(_save);
}

void Terminal::writeLine(const std::string& line)
{

  ++_nbLines;

  _save += line;
  _textSave.setString(_save);

  if(_nbLines >= 14)
  {
    delFirstLine(_save);
  }
}

void Terminal::writeMan(const TYPEBUG& bug)
{
  std::string help = "";
  int nb = 0;

  switch (bug)
  {
    case  FREEZE:
      help = "create lag\ntype flame and click on it\n";
      nb = 2;
      break;
    case  VIRUS:
      help = "sometimes, close your terminal\ntype getpid, click on it\nto get the position(i, j).\ntype kill i j\n";
      nb = 4;
      break;
    case  VPN:
      help = "black screen\ntype call cri\n";
      nb = 2;
      break;
    case  BONUS:
      help = "type the word at the bottom to win points\n";
      nb = 1;
      break;
    case  FAKEINPUT:
      help = "sometimes, write in your terminal\ntype close connection\n";
      nb = 2;
      break;
    case  FLOU:
      help = "Blur the screen\ntype glasses and click on it\n";
      nb = 2;
      break;

  }

  //std::cout << help << std::endl;

  while(_nbLines >= 14 - nb)
  {
    delFirstLine(_save);
  }

  _nbLines += nb;

  _save += help;
  _textSave.setString(_save);
}

void Terminal::clear()
{
  _nbLines = 0;
  _save = "";
  _textSave.setString(_save);
}

void Terminal::fakeInput()
{
  if(_commande.size() < 20)
  {
    Bureau::getBureau()->getSound()->play(SFAKEINPUT);
    _commande += (char)(rand()%26 + 'a');
    _textCommande.setString(_commande);
  }
}
