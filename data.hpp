#ifndef DATA_HPP
#define DATA_HPP

#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/System/String.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include <utility>
#include <array>
#include <jsoncpp/json/json.h>
#include <fstream>

const int framerateMax = 60;

const int wWindow = 1000;
const int hWindow = 500;

//barre tache

const int hBarre = 30;
const int wButton = 100;

//console

const int xConsole = 20;
const int yConsole = 20;
const int wConsole = 250;
const int hConsole = 220;

const int xText = xConsole + 10;
const int yText = yConsole + 190;

const int maxLines = 14;

//affichage

const int xAffichage = 290;

//matrice
const int tailleBug = 100;
const int tailleSol = 50;

const int taille_mat_x = (wWindow - xAffichage) / tailleBug;
const int taille_mat_y = (hWindow - hBarre) / tailleBug;

enum TYPEBUG {FREEZE, VIRUS, VPN, FAKEINPUT, FLOU, BONUS};

#endif
