#ifndef TERMINAL_HPP
#define TERMINAL_HPP

#include "data.hpp"

class Terminal {
  private:
    sf::RenderWindow& _window;

    sf::Texture _textureTerminal;
    sf::Sprite _spriteTerminal;

    sf::Font _font;
    sf::Text _textCommande;
    sf::Text _textSave;


    std::string _commande;
    std::string _save;

    int _nbLines;

  public:

    Terminal(sf::RenderWindow& window);
    virtual ~Terminal ();
    void readCommande(const sf::Event& event);
    void draw(sf::RenderTexture& tx);

    void delFirstLine(std::string& txt);

    void exec();

    void writeHelp();

    void writeLine(const std::string& line);
    void writeMan(const TYPEBUG& bug);

    void clear();

    void fakeInput();
};

#endif
