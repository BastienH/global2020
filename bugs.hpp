#ifndef BUGS_HPP
#define BUGS_HPP


#include "effect.hpp"


class Bug
{
  private:

    sf::RenderWindow& _window;

    sf::Texture _textureBug;
    sf::Sprite _spriteBug;

    Effect * _effect;

    TYPEBUG _type;

  public:

    Bug(sf::RenderWindow& window, const int& i, const int& j, const TYPEBUG& type);
    virtual ~Bug ();

    void tick();
    void draw(sf::RenderTexture& tx);
    void solve();

    const TYPEBUG& getType() const;
};



#endif
