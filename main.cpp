#include "bureau.hpp"

int main(int, char**)
{

  //création de la fenêtre
  sf::RenderWindow window(sf::VideoMode(wWindow, hWindow), "Global");

  //60 fps
  window.setFramerateLimit(framerateMax);

  Bureau b(window);

  // on fait tourner le programme tant que la fenêtre n'a pas été fermée
  while (window.isOpen())
  {

    // effacement de la fenêtre en noir
    window.clear(sf::Color::Black);

    b.tick();

    // fin de la frame courante, affichage de tout ce qu'on a dessiné
    window.display();

    if(b.getNbBug() > 10)
    {
      window.close();
    }
  }

  b.deleteAll();

  return 0;
}
