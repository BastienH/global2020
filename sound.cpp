#include "sound.hpp"


Sound::Sound()
{
  _bufferFreeze.loadFromFile("./assets/sounds/freeze.flac");
  _bufferError.loadFromFile("./assets/sounds/error.flac");
  _bufferConnect.loadFromFile("./assets/sounds/connect.flac");
  _bufferDisconnect.loadFromFile("./assets/sounds/disconnect.flac");
  _bufferFail.loadFromFile("./assets/sounds/fail.flac");
  _bufferOpenTerm.loadFromFile("./assets/sounds/openTerminal.flac");
  _bufferCloseTerm.loadFromFile("./assets/sounds/closeTerminal.flac");
  _bufferFakeConnection.loadFromFile("./assets/sounds/fakeinputconnect.flac");
  _bufferFakeInput.loadFromFile("./assets/sounds/key.flac");

  _soundFreeze.setBuffer(_bufferFreeze);
  _soundError.setBuffer(_bufferError);
  _soundConnect.setBuffer(_bufferConnect);
  _soundDisconnect.setBuffer(_bufferDisconnect);
  _soundFail.setBuffer(_bufferFail);
  _soundOpenTerm.setBuffer(_bufferOpenTerm);
  _soundCloseTerm.setBuffer(_bufferCloseTerm);
  _soundFakeConnection.setBuffer(_bufferFakeConnection);
  _soundFakeInput.setBuffer(_bufferFakeInput);

  //_soundFakeConnection.setVolume(150.f);
}


void Sound::play(const SOUND& s)
{
  // sound;

  //std::cout << s << std::endl;

  switch (s)
  {
    case SFREEZE:
      _soundFreeze.play();
      break;
    case ERROR:
      _soundError.play();
      break;
    case CONNECT:
      _soundConnect.play();
      break;
    case DISCONNECT:
      _soundDisconnect.play();
      break;
    case FAIL:
      _soundFail.play();
      break;
    case OPENTERM:
      _soundOpenTerm.play();
      break;
    case CLOSETERM:
      _soundCloseTerm.play();
      break;
    case FAKECONNECTION:
      _soundFakeConnection.play();
      break;
    case SFAKEINPUT:
      _soundFakeInput.play();
      break;
  }

}
