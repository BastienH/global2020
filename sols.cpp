#include "sols.hpp"

Sol::Sol(sf::RenderWindow& window, const TYPEBUG& type)
: _window(window), _type(type)
{
  switch (type)
  {
    case FREEZE:
      _textureSol.loadFromFile("./assets/solFreeze.png");
      break;
    case VIRUS:
      _textureSol.loadFromFile("./assets/solVirus.png");
      break;
    case VPN:
      _textureSol.loadFromFile("./assets/solVpn.png");
      break;
    case FLOU:
      _textureSol.loadFromFile("./assets/solFlou.png");
      break;
  }

  _spriteSol.setTexture(_textureSol);
}

void Sol::tick()
{

}

void Sol::draw(sf::RenderTexture& tx)
{
  tx.draw(_spriteSol);
}

void Sol::setPosition(const int& x, const int& y)
{
  _spriteSol.setPosition(sf::Vector2f(x, y));
}

const TYPEBUG& Sol::getType() const
{
  return _type;
}
