#ifndef BUREAU_HPP
#define BUREAU_HPP

#include "data.hpp"
#include "terminal.hpp"
#include "bugs.hpp"
#include "sols.hpp"
#include "sound.hpp"

const int popupTimeMax = 120;

class Bureau
{
  private:

    sf::RenderWindow& _window;
    sf::Texture _textureBackground;
    sf::Sprite _spriteBackground;

    sf::Texture _texturePopup;
    sf::Sprite _spritePopup;

    sf::RenderTexture _textureWindow;
    sf::Sprite _spriteWindow;

    static Bureau * _bureau;

    Terminal * _terminal;
    Sol * _solution;

    std::array< std::array< Bug *, taille_mat_y>, taille_mat_x> _matrice;

    int _maxCount, _count;

    //bug résolu

    bool _popup;

    int _popupTime;

    //génération de bugs
    int _nbBugs;

    int _frameBug, _frameBugMax;

    //vpn

    sf::RectangleShape _screen;

    bool _connected;

    //sounds

    Sound  * _sound;

    //score

    int _score;

    sf::Font _font;
    sf::Text _textScore;

    //mots bonus

    std::string _motBonus;

    sf::Text _textMotBonus;

    bool _start, _flou;

  public:

    Bureau(sf::RenderWindow& window);
    virtual ~Bureau ();

    void deleteAll();

    void draw();
    void tick();

    static Bureau * getBureau();

    void getEvents();

    void clickReleased(sf::Event& event);
    void mouseMoved(const sf::Event& event);

    void execCommande(const std::string& commande);

    void setFramerate(const int& frame);
    const int& getFramerate() const;

    void tickBugs();

    void killTerminal();

    void generateBug();

    void disconnect();

    const int& getNbBug() const;

    Sound * getSound();

    const bool& isConnected() const;

    void writeScore();

    void pickNewBonus();

    void fakeInput();

    void flouter();
    void deflouter();

};


void createblur(sf::RenderTexture &renderTexture, int boxSize, int iterations = 1);



#endif
