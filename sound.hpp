#ifndef SOUND_HPP
#define SOUND_HPP

#include "data.hpp"

enum SOUND {SFREEZE, ERROR, CONNECT, DISCONNECT, FAIL, OPENTERM, CLOSETERM, FAKECONNECTION, SFAKEINPUT};

class Sound
{
  private:
    sf::SoundBuffer _bufferFreeze;
    sf::SoundBuffer _bufferError;
    sf::SoundBuffer _bufferConnect;
    sf::SoundBuffer _bufferDisconnect;
    sf::SoundBuffer _bufferFail;
    sf::SoundBuffer _bufferOpenTerm;
    sf::SoundBuffer _bufferCloseTerm;
    sf::SoundBuffer _bufferFakeConnection;
    sf::SoundBuffer _bufferFakeInput;

    sf::Sound _soundFreeze;
    sf::Sound _soundError;
    sf::Sound _soundConnect;
    sf::Sound _soundDisconnect;
    sf::Sound _soundFail;
    sf::Sound _soundOpenTerm;
    sf::Sound _soundCloseTerm;
    sf::Sound _soundFakeConnection;
    sf::Sound _soundFakeInput;

  public:
    Sound();
    virtual ~Sound (){};
    void play(const SOUND& s);
};


#endif
