#ifndef EFFECT_HPP
#define EFFECT_HPP

#include "data.hpp"

class Effect
{
  protected:
    sf::RenderWindow& _window;

  public:
    Effect(sf::RenderWindow& window, const int& maxCount)
    : _window(window), _maxCount(maxCount), _count(maxCount){}
    virtual ~Effect(){}
    virtual void tick() = 0;
    virtual void solve() = 0;

    int _maxCount, _count;
};

class Freeze : public Effect
{
  private:

  public:
    Freeze(sf::RenderWindow& window): Effect(window, 60){};
    virtual ~Freeze(){};
    void tick();
    void solve();
};

class Virus : public Effect
{
  private:
    /* data */

  public:
    Virus(sf::RenderWindow& window): Effect(window, 10 * 60){};
    virtual ~Virus(){};
    void tick();
    void solve();
};

class Vpn : public Effect
{
  private:
    /* data */

  public:
    Vpn(sf::RenderWindow& window): Effect(window, 60){};
    virtual ~Vpn(){};
    void tick();
    void solve();
};


class FakeInput : public Effect
{
  private:
    /* data */

  public:
    FakeInput(sf::RenderWindow& window): Effect(window, 5 * 60){};
    virtual ~FakeInput(){};
    void tick();
    void solve();
};


class Flou : public Effect
{
  private:
    /* data */

  public:
    Flou(sf::RenderWindow& window): Effect(window, 0){};
    virtual ~Flou(){};
    void tick();
    void solve();
};



#endif
