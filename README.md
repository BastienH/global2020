# Jeu réalisé lors de la global game jam de 2020

# Thème : Repair

## But du jeu :
        - Corriger des bugs à l'aide de commandes pour avoir le meilleur score.

## Fin du jeu :
        - 10 bugs à l'écran.

## Tips :
        - Il y a des mots bonus en bas de l'écran.
        - Résoudre un bug et écrire un mot bonus rapporte chacun 1 point.
        - 'help' donne des information sur les commandes possibles.

## Contenu :

Bugs            | Effets                                    | Solution
| ------------- |:-----------------------------------------:| ---------------:|
Freeze          | Provoque du lag                           | flame
Virus           | Ferme le terminal de temps en temps       | getpid puis kill
Vpn             | Blackscreen                               | call cri
Fake input      | Ecrit dans le terminal de temps en temps  | close connection
